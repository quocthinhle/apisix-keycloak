== Setup 
curl http://127.0.0.1:9180/apisix/admin/routes/5 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
  "uri": "/services",
  "plugins":{
    "openid-connect":{
      "client_id": "seenow-app",
      "client_secret": "",
      "discovery": "http://keycloak-identity:8080/realms/seenow/.well-known/openid-configuration",
      "introspection_endpoint": "http://keycloak-identity:8080/realms/seenow/protocol/openid-connect/token/introspect",
      "bearer_only": true,
      "realm": "seenow",
      "use_jwks": true
    }
  },
    "upstream": {
        "type": "roundrobin",
        "nodes": {
            "upstream_1:3000": 1
        }
    }
}'

curl http://127.0.0.1:9080/services -H 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYX2djUExRUG8taHZEa2FJSGprNkJ4Vi1mYVdlWG1GcXYzRWZVSDhPT000In0.eyJleHAiOjE3MTEwOTI3NjcsImlhdCI6MTcxMTA5MjQ2NywianRpIjoiNTYxYzgyZDMtZWZiZC00YzM4LTkzMjgtZjU5ZTZhNGY1MzYyIiwiaXNzIjoiaHR0cDovLzEyNy4wLjAuMTo5MDgwL3JlYWxtcy9zZWVub3ciLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiYzRhZWIyYjktNGFmYi00ZTNlLThjZDMtZjI0MDg2MGU2OTFmIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoic2Vlbm93LWFwcCIsInNlc3Npb25fc3RhdGUiOiI1MDYwODFlNS1mMTk2LTQwYjMtODgwNy0yYTgwYWEwNzBjZWUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIi8qIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJkZWZhdWx0LXJvbGVzLXNlZW5vdyIsIm9mZmxpbmVfYWNjZXNzIiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUiLCJzaWQiOiI1MDYwODFlNS1mMTk2LTQwYjMtODgwNy0yYTgwYWEwNzBjZWUiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInByZWZlcnJlZF91c2VybmFtZSI6ImxxdDEyIn0.FS4znVsSyKNZ6DLxZsZ3XpD5y7sGJcVJc99ksdo0Wk-xIq7dg0Br5lN_3JCxyBbn4P4yFIXMx7TlMlU4RfXCJPnMdtLfyC9vqz-KaDmRBpOpCO_frNtJkq7je4KoCUcOSV3j7ZV0aqAfLT65E2dvuuFeMw7daneu7GGwYUP8Vgvyhf1zDwo8s3i7PCNctRbtxmWl1MUy0AbObFsBsdSKzT_-sicfRZ-c2jCX6vEyo8vE20xVmaz5j-Bka6YfICLFrdlJvMq9VktdTZwO9X2kYKJuI-wQ2mAnziJoUiuBubXt4JuDvZpBuLk9xKLx7PgQY_39UyH4jvfClCuKDjHFcg'
curl http://127.0.0.1:9180/apisix/admin/routes/5 -H 'X-API-KEY: edd1c9f034335f136f87ad84b625c8f1' -X PUT -d '
{
    "uri": "/services",
    "plugins": {

    },
    "upstream": {
        "type": "roundrobin",
        "nodes": {
            "upstream_1:3000": 1
        }
    }
}'
